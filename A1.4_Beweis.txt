Die L�nge der Koeffizienten-Arrays wird immer halbiert, bis nur noch einstellige Zahlen zu multiplizieren sind.
F�r n=2 ist der Algorithmus korrekt, da hier bei (A,B) = ([1,5],[2,3]) gilt:
C=6*5=30
D=2
E=15
returned value = [2,13,15] ==> 2x^2+13x+15
Auf naivem Berechnungsweg kommt man auf dasselbe Ergebnis:
x*2x+3x+10x+3*5 = 2x^2+13x+15.
Da der Algorithmus rekursiv �ber die zu multiplizierenden Arrays l�uft und diese in Arrays der L�nge 2 aufteilt,
ist er auch f�r Arrays der L�nge n, bzw Polynome vom Grad n, geeignet.